package main

import (
	"encoding/json"
	"fmt"
)

var rawJson = []byte(`[
  {
    "id": 1,
    "address": {
      "city_id": 5,
      "street": "Satbayev"
    },
    "Age": 20
  },
  {
    "id": 1,
    "address": {
      "city_id": "6",
      "street": "Al-Farabi"
    },
    "Age": "32"
  }
]`)

func main() {
	var users []User

	for key := range rawJson {
		//fmt.Println(key, string(val))
		if 	rawJson[key] == byte('c') &&
			rawJson[key + 1] == byte('i') &&
			rawJson[key + 2] == byte('t') &&
			rawJson[key + 3] == byte('y') &&
			rawJson[key + 4] == byte('_') &&
			rawJson[key + 5] == byte('i') &&
			rawJson[key + 6] == byte('d') &&
			rawJson[key + 8] == byte(':') {
			if rawJson[key + 10] == byte('"') {
				rawJson[key + 10] = byte(' ')
			}
			if rawJson[key + 12] == byte('"') {
				rawJson[key + 12] = byte(' ')
			}else if rawJson[key + 13] == byte('"') {
				rawJson[key + 13] = byte(' ')
			}else if rawJson[key + 14] == byte('"') {
				rawJson[key + 14] = byte(' ')
			}
		}

		if	rawJson[key] == byte('A') &&
			rawJson[key + 1] == byte('g') &&
			rawJson[key + 2] == byte('e') &&
			rawJson[key + 4] == byte(':') {
			if rawJson[key + 6] == byte('"') {
				rawJson[key + 6] = byte(' ')
			}
			if rawJson[key + 8] == byte('"') {
				rawJson[key + 8] = byte(' ')
			}else if rawJson[key + 9] == byte('"') {
				rawJson[key + 9] = byte(' ')
			}else if rawJson[key + 10] == byte('"') {
				rawJson[key + 10] = byte(' ')
			}
		}
	}

	if err := json.Unmarshal(rawJson, &users); err != nil {
		panic(err)
	}

	for _, user := range users {
		fmt.Printf("%#v\n", user)
	}
}

type User struct {
	ID      int64   `json:"id"`
	Address Address `json:"address"`
	Age     int     `json:"age"`
}

type Address struct {
	CityID int64  `json:"city_id"`
	Street string `json:"street"`
}
